---
title: "Home"
recent: date
nrecent: 30
nlast: 10
---

`tora` is a C++ developer toolkit with a focus on high-performance computing of
scientific applications.

## Install Tora
```bash
curl -L https://orgtora.gitlab.io/install.sh -o install.sh
bash install.sh --prefix <prefix> [--tmp-dir tmp-dir] [--version version]
```
